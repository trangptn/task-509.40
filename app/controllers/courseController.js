const courseModel= require('../models/courseModel');

const mongoose= require('mongoose');

const createCourse = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        coursecode,
        coursename,
        price,
        discountprice,
        duration,
        level,
        coverimage,
        teachername,
        teacherphoto,
        ispopular,
        istrending
    } = req.body;

    // B2: Validate du lieu

    try {
        // B3: Xu ly du lieu

        var newCourse = {
            courseCode: coursecode,
            courseName: coursename,
            price:price,
            discountPrice: discountprice,
            duration: duration,
            level: level,
            coverImage: coverimage,
            teacherName: teachername,
            teacherPhoto: teacherphoto,
            isPopular: ispopular,
            isTrending: istrending
        }

        const result = await courseModel.create(newCourse);

        return res.status(201).json({
            message: "Tao course thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllCourses = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    console.log("Get all du lieu");
    try {
        const result = await courseModel.find();

        return res.status(200).json({
            message: "Lay danh sach course thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "COURSE ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findById(courseid);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin course thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


const updateCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;
    const {
        coursecode,
        coursename,
        price,
        discountprice,
        duration,
        level,
        coverimage,
        teachername,
        teacherphoto,
        ispopular,
        istrending
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateCourse = {};
        if(coursecode) {
            newUpdateCourse.courseCode = coursecode
        }
        if(coursename) {
            newUpdateCourse.courseName = coursename
        }
        if(price) {
            newUpdateCourse.price = price
        }
        if(discountprice)
        {
            newUpdateCourse.discountPrice=discountprice
        }
        if(duration)
        {
            newUpdateCourse.duration=duration
        }
        if(level)
        {
            newUpdateCourse.level=level
        }
        if(coverimage)
        {
            newUpdateCourse.coverImage=coverimage
        }
        if(teachername)
        {
            newUpdateCourse.teacherName=teachername
        }
        if(teacherphoto)
        {
            newUpdateCourse.teacherPhoto=teacherphoto
        }
        if(ispopular)
        {
            newUpdateCourse.isPopular=ispopular
        }
        if(istrending)
        {
            newUpdateCourse.isTrending=istrending
        }


        const result = await courseModel.findByIdAndUpdate(courseid, newUpdateCourse);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin course thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findByIdAndRemove(courseid);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin course thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


module.exports={ createCourse, getAllCourses, getCourseById, updateCourseById,deleteCourseById};

