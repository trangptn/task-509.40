const express= require("express");

const courseRouter=express.Router();

const { createCourse, getAllCourses, getCourseById, updateCourseById,deleteCourseById} = require('../controllers/courseController');

courseRouter.get("/courses",getAllCourses);
courseRouter.post("/courses",createCourse);
courseRouter.get("/courses/:courseid",getCourseById);
courseRouter.put("/courses/:courseid",updateCourseById);
courseRouter.delete("/courses/:courseid",deleteCourseById);

module.exports=courseRouter;