//import thu vien
const express= require('express');
const app= new express();
//import mongo
const mongoose= require('mongoose');

const port=8000;
//import model
const courseModel=require('./app/models/courseModel');

//import router
const courseRouter=require('./app/routers/courseRouter');
app.use( express.json());

mongoose.connect("mongodb://127.0.0.1:27017/course365")
.then(()=>{
    console.log("Connect MongoDb successfully");
})
.catch((err) =>
{
    console.log(err);
})

app.use("/",courseRouter);
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})